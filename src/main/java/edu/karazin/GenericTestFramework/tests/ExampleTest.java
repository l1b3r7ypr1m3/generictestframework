package edu.karazin.GenericTestFramework.tests;

import edu.karazin.GenericTestFramework.pages.examplePackage.ExamplePage;
import org.testng.annotations.Test;

public class ExampleTest extends BaseTest {

    @Test
    public void exampleTestMethod() {
        new ExamplePage(driver).getToExamplePage();
        softAssert.assertTrue(true);
    }
}

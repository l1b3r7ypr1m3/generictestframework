package edu.karazin.GenericTestFramework.util;

import org.testng.Reporter;

import java.io.*;
import java.util.Properties;

public class PropertiesContext {

    private static final String ENV_PROPERTIES = "env";
    private static final String PROPERTIES = "properties";
    private static PropertiesContext instance = new PropertiesContext();
    private Properties envMap = new Properties();
    private Properties gradleMap = new Properties();
    private Properties generalMap = new Properties();
    private Properties properties = new Properties();

    private PropertiesContext() {
        init();
    }

    public static PropertiesContext getInstance() {
        if (instance == null) {
            instance = new PropertiesContext();
        }
        return instance;
    }

    public void init() {
        loadPropertiesFromClasspath(envMap, ENV_PROPERTIES);
        loadGradleProperties(gradleMap);
        loadPropertiesFromClasspath(properties, PROPERTIES);

        generalMap.putAll(envMap);
        generalMap.putAll(gradleMap);
        generalMap.putAll(properties);
        if (System.getProperty("envurl") != null) {
            generalMap.setProperty("envurl", System.getProperty("envurl"));
        }
        if (System.getProperty("browser") != null) {
            generalMap.setProperty("browser", System.getProperty("browser"));
        }
        if (System.getProperty("maxRetryCount") != null) {
            generalMap.setProperty("maxRetryCount", System.getProperty("maxRetryCount"));
        }
        if (System.getProperty("threadcount") != null) {
            generalMap.setProperty("threadcount", System.getProperty("threadcount"));
        }
    }

    public String getProperty(String key) {
        String result = (String) generalMap.get(key);
        if (result != null) {
            return result;
        } else {
            throw new NullPointerException("Property " + key + " was not found");
        }
    }

    public void setProperty(String key, String value) {
        generalMap.setProperty(key, value);
    }

    public void clear() {
        generalMap.clear();
    }

    private String getFullFileName(String fileName) {
        return fileName + ".properties";
    }

    private void loadGradleProperties(Properties props) {
        try {
            String fileName = "gradle.properties";
            String path = System.getProperty("user.dir");
            Reader resource = new FileReader(new File(path + "/" + fileName));
            if (resource != null) {
                props.load(resource);
            }
        } catch (IOException e) {
            Reporter.log("Missing or corrupt property file", true);
        }
    }

    private void loadPropertiesFromClasspath(Properties props, String fileName) {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream resourceAsStream = classLoader.getResourceAsStream(getFullFileName(fileName));


            if (resourceAsStream != null) {
                props.load(resourceAsStream);
            }
        } catch (IOException e) {
            Reporter.log("Missing or corrupt property file", true);
        }
    }

}

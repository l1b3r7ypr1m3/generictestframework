package edu.karazin.GenericTestFramework.pages;


import org.openqa.selenium.WebDriver;
import edu.karazin.GenericTestFramework.util.Helper;
import edu.karazin.GenericTestFramework.util.PropertiesContext;
import edu.karazin.GenericTestFramework.util.SoftAssert;

public abstract class AbstractPage {

    private static final String ENV = PropertiesContext.getInstance().getProperty("envurl");
    protected static PropertiesContext context = PropertiesContext.getInstance();
    public SoftAssert softAssert;
    protected WebDriver driver;
    protected Helper helper;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.helper = Helper.getInstance(driver);
        this.softAssert = SoftAssert.getInstance(driver);
    }

    public void reload() {
        driver.navigate().refresh();
    }

    public AbstractPage navigate() {
        driver.get(ENV);
        return this;
    }

    public WebDriver getDriver() {
        return driver;
    }
}

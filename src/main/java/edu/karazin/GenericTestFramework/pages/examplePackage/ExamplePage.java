package edu.karazin.GenericTestFramework.pages.examplePackage;

import edu.karazin.GenericTestFramework.pages.AbstractPage;
import org.openqa.selenium.WebDriver;

public class ExamplePage extends AbstractPage {
    public ExamplePage(WebDriver driver) {
        super(driver);
    }

    public ExamplePage getToExamplePage() {
        navigate();
        return this;
    }
}



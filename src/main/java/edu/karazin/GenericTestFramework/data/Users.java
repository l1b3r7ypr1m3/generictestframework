package edu.karazin.GenericTestFramework.data;


public enum Users {
    ADMIN("Admin", "Changeit");

    private String name;
    private String password;

    Users(String name, String password) {
        this.name = name;
        this.password = password;

    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

}
